package dawson;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigorous Test :-)
     */

    App app = new App();

    @Test
    public void answerShouldBeFive(){
        assertEquals("this test is to check if the value returned is equal to 5",5, app.echo(5));
    }

    @Test
    public void answerShouldBeSix(){
        assertEquals("this test is to check if the value returned is equal to 6",6, app.oneMore(5));
    }
}
